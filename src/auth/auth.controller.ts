import { Controller, Get, Query, Res } from '@nestjs/common';
import { AuthService } from './auth.service';
import { Response } from 'express';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Get('callback')
  async handleRedirect(@Query('code') code: string, @Res() res: Response) {
    // Aquí manejarías el código recibido y obtendrías el token de acceso
    // const accessToken = await this.authService.exchangeCodeForToken(code);
    // console.log('token> ', accessToken);

    // // Luego puedes redireccionar al usuario o enviar un mensaje, según tu flujo
    // res.redirect('/success');

    try {
      // Intenta obtener el token de acceso
      const accessToken = await this.authService.exchangeCodeForToken(code);
      console.log('token> ', accessToken);

      // Enviar token como respuesta en lugar de redirigir
      return res.json({ accessToken });
    } catch (error) {
      // Manejo de errores
      console.error(error);
      return res.status(500).json({ message: 'Internal Server Error' });
    }
  }
}
