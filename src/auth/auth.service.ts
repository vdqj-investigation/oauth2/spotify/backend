import { Injectable } from '@nestjs/common';
import axios from 'axios';

@Injectable()
export class AuthService {
  private clientId = '88fb9a09f0ae467982a84681b93886ae';
  private clientSecret = 'bd0a30681e00469e9f0c1b3328b1b095';

  async exchangeCodeForToken(code: string): Promise<string> {
    try {
      // Aquí intercambias el código por un token de acceso
      const tokenResponse = await axios({
        method: 'post',
        url: 'https://accounts.spotify.com/api/token',
        params: {
          grant_type: 'authorization_code',
          code: code,
          redirect_uri: 'http://localhost:4200/callback',
        },
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          Authorization: `Basic ${Buffer.from(
            `${this.clientId}:${this.clientSecret}`,
          ).toString('base64')}`,
        },
      });

      console.log('resp: ', tokenResponse);

      return tokenResponse.data.access_token; // O manejar el objeto de respuesta según sea necesario
    } catch (e) {
      console.log(e);
    }
  }
}
